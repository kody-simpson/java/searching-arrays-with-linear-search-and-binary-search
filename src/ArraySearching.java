public class ArraySearching {

    public static void main(String[] args) {

        double[] test_scores = {45.5, 75.6, 100.0, 562.3};

        System.out.println(linearSearch(100.0, test_scores));

        System.out.println(binarySearch(100.0, test_scores));
        System.out.println(binarySearch(75.6, test_scores));

        int[] the_iq_of_each_of_my_brains = {45,23, 67, 214, 25, 15, 7, 336, 2, 525};

        //The above array is unsorted, so it can only be used with the linear search
        System.out.println(linearSearch(7, the_iq_of_each_of_my_brains));


    }

    //Linear Search
    //Checking each element one by one until you find the value
    //you are searching for or reach the end of the array.
    //If you find the value in the array, return its index. If you
    //do not find it, return -1
    public static int linearSearch(double key, double[] values){

        //Make a simple for loop to go through each element of the array
        for (int i = 0; i < values.length; i++){
            //If the value of this element is equal
            //to the key then we have found a matching element
            if (values[i] == key){
                //break out of the loop and return the index
                //of the element where the key was found
                return i;
            }
        }
        //If it reached this point, it was not found
        //so return -1
        return -1;

    }

    //Binary Search - The array must already be sorted (This is will be for an Ascending array)
    //The idea behind this is to start in the middle of the array
    //and see if that element matches. If the key is lower than the
    //middle one, then we know the top half of the array can be eliminated.
    //If the key is higher than the middle element, you can eliminate the
    //bottom half. Then repeat by starting at the middle of the new portion
    //of the array and keep halving it until you find it or don't.
    public static int binarySearch(double key, double[] values){

        int low = 0; //stores the index of the front end
        int high = values.length - 1; //stores the index of the back end
        int mid; //store the index of the middle element

        while (low <= high){
            mid = (high + low) / 2;
            if (key == values[mid]){
                return mid; //if they are equal, we have found the element so return the index
            }else if (key < values[mid]){
                //since the key is lower, change the high index to the middle point
                //which eliminates a good portion of results that we would otherwise
                //need to go through in a linear search
                high = mid;
            }else if (key > values[mid]){
                low = mid;
            }
        }

        return -1;
    }

    //overloaded linearsearch method to work with int array
    public static int linearSearch(double key, int[] values){

        //Make a simple for loop to go through each element of the array
        for (int i = 0; i < values.length; i++){
            //If the value of this element is equal
            //to the key then we have found a matching element
            if (values[i] == key){
                //break out of the loop and return the index
                //of the element where the key was found
                return i;
            }
        }
        //If it reached this point, it was not found
        //so return -1
        return -1;

    }


}
